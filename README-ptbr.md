<p align="center">
  <img src="./src/assets/images/starry.png" width="150" alt="Project Logo">
  <p align="center">⭐⭐⭐⭐⭐</p> 
  <h1 align="center">Starry Sky</h1>
  <p align="center">Aprendendo um pouco sobre canvas, desenhando um belo de um céu noturno :D</p>
  <p align="center">
    <img src="https://img.shields.io/badge/engine-canvas-informational" alt="Animation Engine" />
    <img src="https://img.shields.io/badge/type-experimental-orange" alt="Repo Type" />
    <img src="https://img.shields.io/badge/language-javascript-yellow" alt="Repo Main Language" />
    <img src="https://img.shields.io/badge/platform-web-success" alt="Animation Engine" />
  </p>
  <p align="center">
      <a href="https://www.linkedin.com/in/alexrintt" target="_blank">
        <img src="https://img.shields.io/twitter/url?label=Connect%20%40alexrintt&logo=linkedin&url=https%3A%2F%2Fwww.twitter.com%2Falexrintt%2F" alt="Follow" />
      </a>
  </p>
</p>

<p align="center">
  <img src="/src/assets/images/starry-gif.gif" alt="Demo Image" height="200" />
</p>

<p>
  <img src="./src/assets/images/en.png" alt="Portuguese" height="16">
  <a href="https://github.com/alexrintt/starry-sky/blob/master/README.md">Read in English</a>
</p>

## Mas o que que é isso aqui?

Esse repo é uma [página bem simples](https://alexrintt.github.io/starry-sky) com um canvas em Fullscreen que está dando vida à infinitas de estrelas, gotas de água, ou qualquer coisa que você tenha entendido, no final isso são só linhas mesmo... desenhadas, apagadas e resenhadas novamente.

<br>
<br>
<br>
<br>

<samp>

<h2 align="center">
  Open Source
</h2>
<p align="center">
  <sub>Copyright © 2020-present, Alex Rintt.</sub>
</p>
<p align="center">Starry Sky <a href="https://github.com/alexrintt/starry-sky/blob/master/LICENSE.md">is MIT licensed 💖</a></p>
<p align="center">
  <img src="./src/assets/images/starry.png" width="35" />
</p>

</samp>
